<?php

namespace giftbox\securite;
use giftbox\models\Utilisateur;


define('CLIENT',1);
define('ADMIN',2);
class Authentication {
	public static function createUser ( $username, $password, $verifpass, $email, $verifemail ) {
		$mess="";
		$containsLetter  = preg_match('/[a-zA-Z]/',    $password);
		$containsDigit   = preg_match('/\d/',          $password);
		$passegaux = $password === $verifpass;
		$emailegaux = $email === $verifemail;
		$ok = $containsLetter && $containsDigit;
		$passegaux && $emailegaux;
		
		$pseudopris = Utilisateur::where('username','=',$username)->count();
		if(strlen($username) < 8) {
			$mess = 'Pseudonyme trop court';
		}
		else if(strlen($password) < 8){
			$mess = 'Mot de passe trop court';
		}
		else if(!$ok){
			$mess = 'Votre mot de passe doit contenir au moins une lettre et un chiffre';
		}
		else if(!$passegaux){
			$mess = "Les mots de passe ne sont pas égaux";
		}
		else if(!$emailegaux){
			$mess = "Les emails ne sont pas égaux";
		}
		else if($pseudopris){
			$mess = "Quelqu'un utilise deja ce pseudo, désolé";
		}
		else{
			$sel = bin2hex(openssl_random_pseudo_bytes(64,$strong));
			$sale = $password . $sel;
			$hashed_passwd = password_hash($sale, PASSWORD_DEFAULT);
			$utilisateur = new Utilisateur();
			$utilisateur->username = $username;
			$utilisateur->password = $hashed_passwd;
			$utilisateur->sel = $sel;
			$utilisateur->authlevel = ADMIN;
			$utilisateur->email = $email;
			$utilisateur->save();
			$user = Utilisateur::where('username','=',$username)->first();
			self::loadProfile($user->id);
		}
		return $mess;
		
	}
	public static function authenticate ( $username, $password ) {
		$mess="";
		$user = Utilisateur::where('username','=',$username)->first();
		if(!empty($user)){
			$sale = $password . $user->sel;
			if(password_verify($sale,$user->password)){
				self::loadProfile($user->id);
			}else {
				$mess = "Mauvais password";
			}
		}else {
			$mess = "Mauvais identifiant";
		}
		return $mess;
	}
	private static function loadProfile( $uid ) {
		$user = Utilisateur::where('id','=',$uid)->first();
		unset($_SESSION['profil']);
		$_SESSION['profil'] = array(
			'id' => $user->id,
			'pseudo' => $user->username,
			'authlevel' => $user->authlevel,
		);
	}
	public static function checkAccessRights ( $required ) {
		if(isset($_SESSION['profil'])) {
			if($_SESSION['profil']['authlevel'] >= $required){
				return true;
			}else {
				//throw exeption
			}
		}else {
			//throw exception
		}
		return false;
	 // si Authentication::$profil['level] < $required
	 // throw new AuthException�;
	}
}