<?php

namespace giftbox\vues;
use giftbox\models\Cadeau;
use giftbox\models\Cagnotte;
class VuePanier {
	
	public $liste;
	public $aff;
	

	public function __construct($l, $affichage){
		$this->liste=$l;
		$this->aff=$affichage;
	}
	
	private function voirCoffretCB(){
		$app = \Slim\Slim::getInstance();
		//la fonction basename rend le dernier element de l'url
		$content='<h1>Coffret n°' . basename("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") . '</h1>
		<table class="table">
			<tr>
				<th>Nom</th>
				<th>Prix</th>
				<th>Quantité</th>
				<th>Catégorie</th>
				<th>Vignette</th>

			</tr>';
		foreach($this->liste as $prestation){
			$content .='
								<tr>
								<td><a href="../prestation/' . $prestation->id . '">' . $prestation->nom . '</a></td>
								<td>' . $prestation->prix . '€</td>
								<td>'.$prestation->pivot->qte.'  </td>
								<td>' . $prestation->categorie->nom . '</td>
								<td> <a href="../prestation/' . $prestation->id . '"><img src="../public/img/' . $prestation->img . '" style=" height: 150px; width: 200px;" alt="' . $prestation->img . '"/></a></td>
								</tr>';
								
		}
		
		$cadeau = Cadeau::where('coffret_id','=',basename("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"))->first();
		if($cadeau==null){
			$content.='</table><h3>Ce coffret a été payé par carte bancaire, votre cadeau peut être généré dès maintenant. </h3>';
			$content .='<p style="font-size:18px">Vous pouvez rendre votre cadeau disponible à partir d\'une date choisie ou bien permettre au récepteur de découvrir le contenu de ce cadeau prestation par prestation.</p>';
			$content.='<h3>Choisir une date de découverte :</h3>
			<form action="../generation-cadeau" method="post">
				<input type="date" name="date" required>
				<button class="btn btn-lg btn-primary btn-success" type="submit">Créer mon cadeau</button><h3>ou</h3>
			</form><a href="../generation-cadeau"><button type="button" class="btn btn-lg btn-success">Créer une séquence cadeau</button></a>';
		
		}else{
			$content.='</table><p style="font-size:18px;">Votre cadeau a bien été généré, voici sont état actuel: <B>'. $cadeau->etat . '</B><br/>';
			//la fonction dirname fait l'inverse de basename, donc elle retire le dernier élément de l'url
			$content.='Voici le lien de votre cadeau : '. "<a href=\"http://$_SERVER[HTTP_HOST]" .dirname($app->urlFor('cadeau')).'/'. $cadeau->id . '" >'. "http://$_SERVER[HTTP_HOST]" .dirname($app->urlFor('cadeau')).'/'. $cadeau->id  .'</a></p>';
		}
		return $content;
	}
	
	private function voirCoffretCagnotte(){
		$app = \Slim\Slim::getInstance();
		$content='<h1>Coffret n°' . basename("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") . '</h1>
		<table class="table">
			<tr>
				<th>Nom</th>
				<th>Prix</th>
				<th>Quantité</th>
				<th>Catégorie</th>
				<th>Vignette</th>

			</tr>';
		foreach($this->liste as $prestation){
			$content .='
								<tr>
								<td><a href="../prestation/' . $prestation->id . '">' . $prestation->nom . '</a></td>
								<td>' . $prestation->prix . '€</td>
								<td>'.$prestation->pivot->qte.'  </td>
								<td>' . $prestation->categorie->nom . '</td>
								<td> <a href="../prestation/' . $prestation->id . '"><img src="../public/img/' . $prestation->img . '" style=" height: 150px; width: 200px;" alt="' . $prestation->img . '"/></a></td>
								</tr>';
								
		}
		$content.='</table>';
		//on vérifie si le cadeau exist
		$cadeau = Cadeau::where('coffret_id','=',basename("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"))->first();
		if($cadeau != null){
			$content.='<p style="font-size:18px;">Votre cadeau a bien été généré, voici sont état actuel: <B>'. $cadeau->etat . '</B><br/>';
			//la fonction dirname fait l'inverse de basename, donc elle retire le dernier élément de l'url
			$content.='Voici le lien de votre cadeau : '. "<a href=\"http://$_SERVER[HTTP_HOST]" .dirname($app->urlFor('cadeau')).'/'. $cadeau->id . '" >'. "http://$_SERVER[HTTP_HOST]" .dirname($app->urlFor('cadeau')).'/'. $cadeau->id  .'</a><br />';
			//recuperation de la cagnote
			$cagnotte=Cagnotte::where('coffret_id','=',basename("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"))->first();
			if($cagnotte != null){
				$content.='<p style="font-size:18px">Cliquez <a href="../cagnotte/gestion/' . $cagnotte->id.  '">ici</a> pour voire la cagnotte de ce coffret.<br />
					Voici l\'ancien lien de participation de la cagnotte: ' . "<a href=\"http://$_SERVER[HTTP_HOST]" .dirname($app->urlFor('participation')).'/'. $cagnotte->id . '" >'. "http://$_SERVER[HTTP_HOST]" .dirname($app->urlFor('participation')).'/'. $cagnotte->id  .'</a></p>';
				
			}
			$content.='</p>';
		}
		else{
		//on verfie l'état de la cagnote
		$cagnotte=Cagnotte::where('coffret_id','=',basename("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"))->first();
			if($cagnotte != null){
				if($cagnotte->etat=='cloture'){
					$content.='<h3>La cagnotte de ce coffret est colturé, votre cadeau est pret à être généré.</h3>';
					$content .='<p style="font-size:18px">Vous pouvez rendre votre cadeau disponible à partir d\'une date choisie ou bien permettre au récepteur de découvrir le contenu de ce cadeau prestation par prestation.</p>';
					$content.='<h3>Choisir une date de découverte :</h3>
					<form action="../generation-cadeau" method="post">
					<input type="date" name="date">
					<button class="btn btn-lg btn-primary btn-success" type="submit">Créer mon cadeau</button><h3>ou</h3>
					</form><a href="../generation-cadeau"><button type="button" class="btn btn-lg btn-success">Créer une séquence cadeau</button></a>';
				}
				else{
					$content.='<p style="font-size:18px">Une cagnotte est actuellement ouverte pour votre coffret, cliquez <a href="../cagnotte/gestion/' . $cagnotte->id.  '">ici</a> pour gerer cette cagnotte.<br />
					Voici le lien de participation de votre cagnotte: ' . "<a href=\"http://$_SERVER[HTTP_HOST]" .dirname($app->urlFor('participation')).'/'. $cagnotte->id . '" >'. "http://$_SERVER[HTTP_HOST]" .dirname($app->urlFor('participation')).'/'. $cagnotte->id  .'</a></p>';
				}
			}
		}
		return $content;
	}
	
	private function validation(){
		$content = '<h2> Valider votre commande </h2>
					<form action="validation" method="post">
						<div style="font-size:18px;">
							<div style="width:130px; display:inline-block;">Votre nom : </div><input type="text" name="nom" required/><br />
							<div style="width:130px; display:inline-block;">Votre prénom : </div><input type="text" name="prenom" required/><br />
							<div style="width:130px; display:inline-block;">Votre email : </div><input type="email" name="email" required/><br />
							<div style="width:130px; display:inline-block;">Votre message : </div><textarea name="message" cols="50" rows="10" required></textarea><br />
							Mode de paiement : 
							<input type="radio" name="modePaiement" value="classique" id="classique" onclick="paiement(this.checked)" checked/> Classique
							<input type="radio" name="modePaiement" value="cagnotte" id="cagnotte" onclick="paiement(!this.checked);"/>Cagnote<br />
							<script language="javascript">
								function paiement(Carte)
								{
									document.getElementById(\'numCB\').disabled = !Carte;
									document.getElementById(\'date\').disabled = !Carte;
									document.getElementById(\'cryptogramme\').disabled = !Carte;
									if(Carte){
										document.getElementById(\'mode\').innerHTML = \'Paiement par carte\';
									}
									else{
										document.getElementById(\'mode\').innerHTML = \'Paiement par cagnote\';
									}
								}
							</script>
							<div style="width:130px; display:inline-block;">N° de carte : </div><input id="numCB" type="text" name="numCB"/><br />
							<div style="width:130px; display:inline-block;">Date d\'expiration : </div><input id="date" type="text" name="date"/><br />
							<div style="width:130px; display:inline-block;">Cryptogramme : </div><input id="cryptogramme" type="text" name="cryptogramme"/><br />
							<div style="width:130px; display:inline-block;">Mot de passe* : </div><input type="text" name="mdp"/><br />
							<h2>Récapitilatif de votre commande : </h2>
							<table class="table">
								<tr>
									<th>Nom</th>
									<th>Prix</th>
									<th><center>Quantité</center></th>
									
								</tr>';
							$total = 0;
							
							foreach($this->liste as $prestation=>$qte){
								$element=unserialize($prestation);
								$content .='
								<tr>
								<td>' . $element->nom . '</td>
								<td>' . $element->prix . '€</td>
								<td><center>'.$qte.'  </center></td>
								
								</tr>';
								$total+=$element->prix*$qte;
							}
							$content.='</table>';
							$content.='<h2>Montant total : ' . $total . '€</h2>';
							$content.='<div id="mode">Paiement par carte</div>
							<button class="btn btn-lg btn-primary btn-success" type="submit">Valider</button>
						</div><br />
						*Le mot de passe est optionnel, il permet de protéger l\'accès à la gestion de votre coffret une fois validé.
					</form>';
					
		return $content;
	}
	
	private function afficherCoffret() {
		$montant=0;
		if(!isset($_SESSION['panier']) || count($_SESSION['panier']) <= 0){
			$content='
			<h1>Votre panier :</h1></br>';
			
		}
		else{
			$content='
			<h1>Votre panier :</h1></br>
			<h2>Liste des prestations</h2>
				<table class="table table-striped">
				<tr>
					
					<th>Nom</th>
					<th>Prix</th>
					<th>Quantité</th>
					<th>Catégorie</th>
					<th>Vignette</th>
					<th></th>
				</tr>';
				
			
			
			//liste pour enregistrer le nombre de fois qu'une prestation est commandée
			$liste_qt=array();
			/*for ($i = 1; $i <= 25; $i++){
				$liste_qt[]=0;
			}*/
			foreach($_SESSION['panier'] as $element){
					$prest = unserialize($element);
					if(isset($liste_qt[$prest->id])){
						$liste_qt[$prest->id]++;
					}
					else{
						$liste_qt[$prest->id]=1;
					}
			}
		
			
			$liste_nom=array();
				foreach($_SESSION['panier'] as $element){
					$prest = unserialize($element);
					
					if (in_array($prest->nom,$liste_nom)){
						$montant+=$prest->prix;
					}
					else{
						$liste_nom[]=$prest->nom;
						$lien = 'prestation/'.$prest->id;
						$montant+=$prest->prix;
						//echo $prest->nom . '<br />';
						$content .='
							<tr>
								<td><a href="prestation/' . $prest->id . '">' . $prest->nom . '</a></td>
								<td>' . $prest->prix . '€</td>
								<td><center>'.$liste_qt[$prest->id].'  </center></td>
								<td>' . $prest->categorie->nom . '</td>
								<td> <a href="prestation/' . $prest->id . '"><img src="public/img/' . $prest->img . '" style=" height: 150px; width: 200px;" alt="' . $prest->img . '"/></a></td>
								<td><form action="suppression" method="post">
										<button class= "btn btn-default" name="bouton" value="' . $prest->id . '">Retirer une fois du panier</button>
									</form>
								</td>
							</tr>';
				}
			}
		}
		$content.= '</table>
					<h2>Total à payer: '. $montant . '€ </h2>
					<a href="validation"><button type="button" class="btn btn-lg btn-success">Valider votre panier</button></a>';
					
		return $content;
	}
	
	private function afficherVerification(){
		$idCoffret = $this->liste->id;
		$content = '<h1>Vérification</h1><h3>Le coffret n° ' . $idCoffret . ' est protégé par un mot de passe, veuillez le saisir pour continuer</h3>
			<form action="./' . $idCoffret . '" method="post">
				<p style="font-size:25px;">Mot de passe : <input type="password" name="mdp"/></p>
				<button class="btn btn-lg btn-primary btn-success" type="submit">Entrer</button>
			</form>
		';
		return $content;
		
	}
	
	public function render() {
		switch($this->aff){
			case AFF_COURANT:
			$content = $this->afficherCoffret();
			$racine="";
			break;
			case AFF_VALIDATION:
			$content = $this->validation();
			$racine="";
			break;
			case AFF_COFFRET_CB:
			$content = $this->voirCoffretCB();
			$racine="../";
			break;
			case AFF_COFFRET_CAGNOTTE:
			$content = $this->voirCoffretCagnotte();
			$racine="../";
			break;
			case AFF_VERIFICATION:
			$content = $this->afficherVerification();
			$racine="../../";
			break;
			default:
			$content = $this->afficherAccueil();
			$racine="";
			break;
		}
		
		$log = "";
		$ajout = "";
		if(!isset($_SESSION['profil'])){
			$log = '<li><span class="navbar-text"><a href='.$racine.'inscription> Inscrivez vous</a></span></li><li><span class="navbar-text"><a href='.$racine.'connexion> Connectez vous</a></span></li>';
		}else {
			$log = '<li><span class="navbar-text">Bienvenue '.$_SESSION['profil']['pseudo'].'</span></li><li><span class="navbar-text"><a href='.$racine.'deconnexion>Se deconnecter</a></span></li>';
			$ajout = '<li><a href="'.$racine.'ajouterPrestation">Ajout</a></li>';

		}
		
		$html =<<<END
		<!DOCTYPE html>
		<html>
		<head> 
			<link href="{$racine}public/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all" type="text/css">
			<link href="{$racine}public/Bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" media="all" type="text/css">
			<title>GIFTBOX</title> 
		</head>
		<body>
		<div class="container" style="padding-top:60px">
		<nav class="navbar navbar-default navbar-fixed-top">
		  <div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <span class="navbar-brand">GIFTBOX</span>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">
				<li><a href="{$racine}.">Accueil</a></li>
				<li><a href="{$racine}prestations">Prestations</a></li>
				<li><a href="{$racine}categories">Catégories</a></li>
				{$ajout}
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
			  {$log}
				<li><a href="{$racine}coffret">Panier</a></li>
				
			  </ul>
			</div><!--/.nav-collapse -->
		  </div>
		</nav>
		 $content
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="{$racine}public/Bootstrap/dist/js/bootstrap.min.js"></script>
		</body><html>
END;
	return $html;
	}
}