<?php 
namespace giftbox\vues;

/*define('AFF_PRESTATIONS',1);
define('AFF_PRESTATION_PAR_ID',2);
define('AFF_CATEGORIES',3);
define('AFF_PRESTATIONS_PAR_CATEGORIES',4);*/
use giftbox\models\Categorie;
use giftbox\models\Prestation;
use giftbox\models\Notations;
use Illuminate\Database\Capsule\Manager as DB;
use giftbox\securite\Authentication;


class VueCatalogue{
	
	public $liste;
	public $aff;
	

	public function __construct($l, $affichage){
		$this->liste=$l;
		$this->aff=$affichage;
	}
	
	private function afficherPrestations(){
		
		$res = '<h1 >Liste des Prestations
		<div class="btn-group">
  <button class="btn">Tri</button>
  <button class="btn dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> </button>
  <ul class="dropdown-menu">
    <li><a href="./croissant">Prix croissant</a></li>
    <li><a href="./decroissant">Prix decroissant</a></li>
   </ul>
</div></h1><hr/>';
		foreach($this->liste as $l){
            $lien='prestation/'.$l->id;
			$note_moy = round(Notations::where('prest_id','=',$l->id)->avg('note'),1);
			$note_moy_arrondie = round($note_moy);
			$nb = Notations::where('prest_id','=',$l->id)->count('note');
			$etoiles = "";
			$s = "s";
			if($nb == 1){
				$s = "";
			}
			if($nb > 0) {
				$texte_moy = 'Note des utilisateurs : '. $note_moy.' sur un échantillon de '.$nb.' note'.$s;
				for($i=0;$i<$note_moy_arrondie;$i++){
					$etoiles.='<img  src="public/img/star-on.svg" alt="star-on" style=" height: 30px;"/>';
				}
				for($i=$note_moy_arrondie;$i<5;$i++){
					$etoiles.='<img  src="public/img/star-off.svg" alt="star-off" style=" height: 30px;"/>';
				}
			}
			else{
				$texte_moy='Jamais noté';
			}
			$boutonSupprimer = "";
			if(Authentication::checkAccessRights(2)){
				$boutonSupprimer = '<a class="btn btn-xs btn-danger" style="margin-top:3px;margin-right:10px;"  href="supprimerPrestation/'.$l->id.'">Supprimer</a>';
			}
			$boutonDesactiver = "";
			if(Authentication::checkAccessRights(2)){
				$boutonDesactiver = '<a class="btn btn-xs btn-info" style="margin-top:3px;" href="desactivePrestation/'.$l->id.'">Desactiver</a>';
			}
			$boutonActiver = "";
			if(Authentication::checkAccessRights(2)){
				$boutonActiver = '<a class="btn btn-xs btn-info" style="margin-top:3px;" href="activePrestation/'.$l->id.'">Activer</a>';
			}
            $res.= '<div class="well">
						<a href="'.$lien.'" style="text-decoration: none;"> 
						<img src="public/img/'.$l->img.'" alt="' . $l->img . '" style=" height: 110px; min-width: 120px; float:right; border-color:black; border-style:solid; border-width:1px;" />
						<h3 style="margin-top:0px;">'.$l->nom.'</h3>
						<p>'.$l->categorie->nom.'<br /> ' . $l->prix . '&euro;</p>
						
						<p style="color:black; margin-bottom:0px;">'.$texte_moy.'<br/>'.$etoiles.'</p></a>'.$boutonSupprimer ;
						if($l->etat=='inactive'){
							$res.=$boutonActiver;
						}else {
							$res.=$boutonDesactiver;
						}
						$res.='</div><hr/>';
		}
		return $res;
	}
	
	//Methode correspondant à la fonctionnalité n°2
	private function afficherPrestation(){
		//Calcul de la moyenne des notes et créations du bloc étoiles
		$note_moy = round(Notations::where('prest_id','=',$this->liste->id)->avg('note'),1);
		$note_moy_arrondie = round($note_moy);
		$nb = Notations::where('prest_id','=',$this->liste->id)->count('note');
		$etoiles = "";
		$s = "s";
		if($nb == 1){
			$s = "";
		}
		if($nb > 0) {
			$texte_moy = $note_moy.' sur un échantillon de '.$nb.' note'.$s;
			for($i=0;$i<$note_moy_arrondie;$i++){
				$etoiles.='<img  src="../public/img/star-on.svg" alt="star-on" style=" height: 30px;"/>';
			}
			for($i=$note_moy_arrondie;$i<5;$i++){
				$etoiles.='<img  src="../public/img/star-off.svg" alt="star-off" style=" height: 30px;"/>';
			}
		}
		else{
			$texte_moy='Jamais noté';
			$etoiles ='<img  src="../public/img/star-off.svg" alt="star-off" style=" height: 30px;"/><img  src="../public/img/star-off.svg" alt="star-off" style=" height: 30px;"/><img  src="../public/img/star-off.svg" alt="star-off" style=" height: 30px;"/><img  src="../public/img/star-off.svg" alt="star-off" style=" height: 30px;"/><img  src="../public/img/star-off.svg" alt="star-off" style=" height: 30px;"/>';
		}
		$boutonSupprimer = "";
			if(Authentication::checkAccessRights(2)){
				$boutonSupprimer = '<a class="btn btn-danger" href="./../supprimerPrestation/'.$this->liste->id.'">Supprimer<a>';
			}
		//on regarde si on a déjà voté
		$vote = '<p>Votre note: 		  
				<form action="../note/'.$this->liste->id.'" method="post" class="starRating">
					<input id="rating5" type="radio" name="rating" value="5" onclick="this.form.submit();">
					<label for="rating5">5</label>
					<input id="rating4" type="radio" name="rating" value="4" onclick="this.form.submit();">
					<label for="rating4">4</label>
					<input id="rating3" type="radio" name="rating" value="3" onclick="this.form.submit();">
					<label for="rating3">3</label>
					<input id="rating2" type="radio" name="rating" value="2" onclick="this.form.submit();">
					<label for="rating2">2</label>
					<input id="rating1" type="radio" name="rating" value="1" onclick="this.form.submit();">
					<label for="rating1">1</label>
				</form>
			</p>';
		if(isset($_SESSION["notes"])){
			if(isset ($_SESSION["notes"][$this->liste->id])){
				$vote = "<p>Merci d'avoir voté !</p>";
			}
		}
		
		
		//resultat a afficher
		$res=
			'<h1 align="center">'.$this->liste->nom.'  </h1> <div class="jumbotron">
				<img  src="../public/img/' .$this->liste->img .'"style="width:100%;" alt="'.$this->liste->img.'" />
			<br/><br/>
			<p style="font-size:25px;">Description : <br />
			'.$this->liste->descr . '
			</br></br>
			<B>Prix : </B>
			'.$this->liste->prix . ' €
			</br></br>
			<B>ID de la prestation : </B>' . $this->liste->id . '<br />' .
			'<B>Catégorie de la prestation : </B> <a href="../prestations/'. $this->liste->categorie->nom . '">' . $this->liste->categorie->nom . '</a></p>'
			.'<p align="center">
			<p>Note moyenne des utilisateurs : '.$texte_moy.'<br/>'.$etoiles.$vote.'
			<form action="../ajout" method="post">
				<button class= "btn btn-lg btn-success" name="bouton" value="' . $this->liste->id . '">Ajouter au panier</button>
			</form>'.$boutonSupprimer.'<br /><p>';
		return $res ;
	}
	
	private function afficherCategories(){
		$res='	<h1>Liste des Catégories</h1>';
		
		foreach($this->liste as $l){
			$res.= '<hr><div class="row">
            <div class="col-xs-12">
                <a href="./prestations/'.$l->nom.'" style="text-decoration:none;font-size:25px;"><div class="well">'.$l->nom.'</div></a>
            </div>
        </div>
        ';
        
		}
		return $res;
	}
	
	private function afficherPrestParCat(){
		
		$res='<div><h1>Prestations de la catégorie ' . htmlspecialchars(urldecode(basename("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"))) . '</h1><hr/><div class="btn-group">
  <button class="btn">Tri</button>
  <button class="btn dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> </button>
  <ul class="dropdown-menu">
    <li><a href="./croissant/' . htmlspecialchars(urldecode(basename("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"))) . '">Prix croissant</a></li>
    <li><a href="./decroissant/' . htmlspecialchars(urldecode(basename("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"))) . '">Prix decroissant</a></li>
   </ul>
</div></div>';
		foreach($this->liste as $l){
			$note_moy = round(Notations::where('prest_id','=',$l->id)->avg('note'),1);
			$note_moy_arrondie = round($note_moy);
			$nb = Notations::where('prest_id','=',$l->id)->count('note');
			$etoiles = "";
			if($nb > 0) {
				$texte_moy = 'Note des utilisateurs : '. $note_moy.'/5';
				for($i=0;$i<$note_moy_arrondie;$i++){
					$etoiles.='<img  src="../public/img/star-on.svg" alt="star-on" style=" height: 30px;"/>';
				}
				for($i=$note_moy_arrondie;$i<5;$i++){
					$etoiles.='<img  src="../public/img/star-off.svg" alt="star-off" style=" height: 30px;"/>';
				}
			}
			else{
				$texte_moy='Jamais noté';
			}
        $lien='../prestation/'.$l->id;
		$res.= '<div class="well">
				<a href="'.$lien.'" style="text-decoration: none;"> 
				<img src="../public/img/'.$l->img.'" alt="' . $l->img . '" style=" height: 110px; min-width: 120px; float:right; border-color:black; border-style:solid; border-width:1px;" />
				<h3 style="margin-top:0px;">'.$l->nom.'</h3>
				<p>'.$l->categorie->nom.'<br /> ' . $l->prix . '&euro;</p>	
				<p style="color:black; margin-bottom:0px;">'.$texte_moy.'<br/>'.$etoiles.'</p></a>
				</div><hr/>';
		}
		return $res;
	}
	
	private function afficherAccueil(){
		$html = '
					<h1>Bienvenue sur GIFTBOX </h1>
				<hr/>
				<div class="row">';
		
		foreach($this->liste as $i => $prest){
			if(isset($this->liste[$i])){
				$note_moy = round(Notations::where('prest_id','=',$prest->id)->avg('note'),1);
				$note_moy_arrondie = round($note_moy);
				$nb = Notations::where('prest_id','=',$prest->id)->count('note');
				$etoiles = "";
				$s = "s";
				if($nb == 1){
					$s = "";
				}
				if($nb > 0) {
					$texte_moy = 'Note des utilisateurs : '. $note_moy.'/5';
					for($i=0;$i<$note_moy_arrondie;$i++){
						$etoiles.='<img  src="public/img/star-on.svg" alt="star-on" style=" height: 30px;"/>';
					}
					for($i=$note_moy_arrondie;$i<5;$i++){
						$etoiles.='<img  src="public/img/star-off.svg" alt="star-off" style=" height: 30px;"/>';
					}
				}
				else{
					$texte_moy='Jamais noté';
				}
				$lien='prestation/'.$prest->id;
				$html.=' <div class="col-sm-6 col-md-3" >
							<h3>'.$prest->categorie->nom.'</h3>
							<div class="thumbnail" style="min-height:500px;">
							  <img style="height:180px;" src="public/img/'.$prest->img.'" alt="'.$prest->nom.'">
							  <div class="caption">
								<h2>'.$prest->nom.'</h2>'.$texte_moy.'<br/>'.$etoiles.'
								<h3>Description:</h3><p>'.$prest->descr.'<br/><a href="'.$lien.'"">Plus de détails</a></p>
								<p>
									
									<form action="ajout" method="post">
										<button class= "btn btn-success" name="bouton" value="' . $prest->id . '">Ajouter au panier</button>
									</form>								
							  </div>
							</div>
						  </div>';
			}
		}
		$html.='</div>';
		return $html;
	}
	
	private function afficherFormulaire() {
		$categ = '<SELECT name="categorie" size="1">';

		$cats = Categorie::get();
		foreach($cats as $cat){
			$categ .='<option value="'.$cat->nom.'">'.$cat->nom.'</option>';
		}
		$categ .= '</SELECT>';
		
		$html = '<h1>Ajouter une prestation </h1>
		<hr/>
		<h3>Indiquez les informations de la nouvelle prestation</h3>
		<form action="ajouterPrestation" method="post" enctype="multipart/form-data">
			<div style="font-size:18px;">
				<div style="width:130px; display:inline-block;">Nom : </div><input type="text" name="nom" required/><br />
				<div style="width:130px; display:inline-block;"> Categorie : </div>'.$categ.
				'<br/>
				<div style="width:130px; display:inline-block;">Prix : </div><input type="number" name="prix" step="0.01" min="0.01" required/>€<br />
				<div style="width:130px; display:inline-block;">Description : </div><textarea name="description" cols="50" rows="3" required></textarea><br />
				<div style="width:130px; display:inline-block;">Image :  </div><input type="file" name="image" /><br />
				<button class="btn btn-lg btn-primary btn-success" type="submit">Valider</button>
			</div>
		</form>';

		return $html;
	}
	
	
	
	public function render(){
		switch($this->aff){
			case AFF_PRESTATIONS:
			$content = $this->afficherPrestations();
			$racine="";
			break;
			case AFF_PRESTATION_PAR_ID:
			$content = $this->afficherPrestation();
			$racine="../";
			break;
			case AFF_CATEGORIES:
			$content = $this->afficherCategories();
			$racine="";
			break;
			case AFF_PRESTATIONS_PAR_CATEGORIES:
			$content = $this->afficherPrestParCat();
			$racine="../";
			break;
			case AFF_FORMULAIRE_NOUVELLE_PRESTATION:
			$content = $this->afficherFormulaire();
			$racine="";
			break;
			default:
			$content = $this->afficherAccueil();
			$racine="";
			break;
		}
		
		$log = "";
		$ajout ="";
		if(!isset($_SESSION['profil'])){
			$log = '<li><span class="navbar-text"><a href='.$racine.'inscription> Inscrivez vous</a></span></li><li><span class="navbar-text"><a href='.$racine.'connexion> Connectez vous</a></span></li>';

		}else {
			$log = '<li><span class="navbar-text">Bienvenue '.$_SESSION['profil']['pseudo'].'</span></li><li><span class="navbar-text"><a href='.$racine.'deconnexion>Se deconnecter</a></span></li>';
			$ajout = '<li><a href="'.$racine.'ajouterPrestation">Ajout</a></li>';
		}
		
		$html =<<<END
<!DOCTYPE html>
<html>
<head> 
	<link href="{$racine}public/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all" type="text/css">
	<link href="{$racine}public/Bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" media="all" type="text/css">
	<link href="{$racine}stylesheet.css" rel="stylesheet" media="all" type="text/css">
	<title>GIFTBOX</title> 
</head>
<body>

	<div class="container" style="padding-top:60px">
		<nav class="navbar navbar-default navbar-fixed-top">
		  <div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <span class="navbar-brand" href="#">GIFTBOX</span>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">
				<li><a href="{$racine}.">Accueil</a></li>
				<li><a href="{$racine}prestations">Prestations</a></li>
				<li><a href="{$racine}categories">Catégories</a></li>
					{$ajout}
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
			    {$log}
				<li><a href="{$racine}coffret">Panier</a></li>
				
			  </ul>
			</div><!--/.nav-collapse -->
		  </div>
		</nav>

			$content
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="{$racine}public/Bootstrap/dist/js/bootstrap.min.js"></script>
</body>
<html>
END;
		return $html;
	}
}