<?php

namespace giftbox\vues;
use giftbox\models\Cadeau;
class VueCadeau {
	
	public $liste;
	public $aff;
	
	public function __construct($l, $affichage){
		$this->liste=$l;
		$this->aff=$affichage;
	}
	
	private function debuterVueSequence(){
		$cadeau = Cadeau::where('id','=',$_SESSION['idCadeau'])->first();
		$cadeau->etat='En cours d\'ouverture';
		$cadeau->save();
		$content = '<h1>' . ' ' . $this->liste['prenom'] . ' ' . $this->liste['nom'] . ' vous a offert un cadeau!</h1>
		<p style= "font-size:18px;"> Voici son message : <br />' . nl2br($this->liste['message']) . '</p>
		Pour visualiser le contenu de ce cadeau cliquez sur "Suivant" jusqu\'à avoir découvert tout son contenu<br />
		<a href="../decouvrir-cadeau"><button type="button" class="btn btn-lg btn-success">Suivant >></button></a>';
		return $content;
	}
	
	private function avancerDansSequence(){
		$content = '<h1 align="center">'.$this->liste->nom.'  </h1> <div class="jumbotron">
		<img  src="public/img/' .$this->liste->img .'"style="width:100%;" alt="'.$this->liste->img.'" />
		<br/><br/>
			<p style="font-size:25px;">Description : <br />
			'.$this->liste->descr . '
			</br></br>
			Cette prestation vous a été offerte '. $_SESSION['cadeaux'][$_SESSION['indiceCadeau']]['qte'] . ' fois <br />';
			if($_SESSION['indiceCadeau'] == count($_SESSION['cadeaux'])-1){
				unset($_SESSION['cadeaux']);
				unset($_SESSION['indiceCadeau']);
				//modification de l'état du cadeau
				$cadeau = Cadeau::where('id','=',$_SESSION['idCadeau'])->first();
				$cadeau->etat='Entièrement ouvert';
				$cadeau->save();
				$content.= 'Félicitation ! Vous avez entièrement ouvert votre cadeau.</div>
				<a href="./">Retour à l\'accueil</a>';
			}else{
				$content.='<a href="decouvrir-cadeau"><button type="button" class="btn btn-lg btn-success">Suivant >></button></a>
				</div>';
				$_SESSION['indiceCadeau']++;
			}
		
		return $content;
	}
	
	private function cadeauNonDisponible(){
		$content = '<h1>' . ' ' . $this->liste['prenom'] . ' ' .$this->liste['nom'] . ' vous a offert un cadeau!</h1>
		<p style= "font-size:18px;"> Voici son message : <br />' . nl2br($this->liste['message']) . '<br />
		Comme c\'est une surprise, ce cadeau vous sera dévoilé à une date inconnue, n\'hésitez pas a revenir demain!';
		return $content;
	}
	
	private function afficherEnsemble(){ //amélioré l'interface svp
		$content = '<h1>' . ' ' . $this->liste['prenom'] . ' ' .$this->liste['nom'] . ' vous a offert un cadeau!</h1>
		<p style= "font-size:18px;"> Voici son message : <br />' . nl2br($this->liste['message']) . '<br /><table class="table">
			<tr>
				<th>Prestation</th>
				<th>Quantité</th>
				<th>Catégorie</th>
				<th>Vignette</th>
				<th>Description</th>
			</tr>';
		foreach($this->liste['prestations'] as $prestation){
			$content .='
								<tr>
								<td><a href="../prestation/' . $prestation->id . '">' . $prestation->nom . '</a></td>
								<td>'.$prestation->pivot->qte.'  </td>
								<td>' . $prestation->categorie->nom . '</td>
								<td> <a href="../prestation/' . $prestation->id . '"><img src="../public/img/' . $prestation->img . '" style=" height: 150px; width: 200px;" alt="' . $prestation->img . '"/></a></td>
								<td>' . $prestation->descr . '</td>
								</tr>';
								
		}
		$content.='</table>';
		return $content;
		
	}
	public function render(){
		switch($this->aff){
			case AFFICHER_DEBUT_SEQUENCE:
			$content = $this->debuterVueSequence();
			$racine="../";
			break;
			case AFFICHER_SEQUENCE:
			$content = $this->avancerDansSequence();
			$racine='./';
			break;
			case AFFICHER_ENSEMBLE:
			$content = $this->afficherEnsemble();
			$racine="../";
			break;
			case CADEAU_NON_DISPONIBLE:
			$content = $this->cadeauNonDisponible();
			$racine="../";
			break;
		}
		
		$log = "";
		$ajout = "";
		if(!isset($_SESSION['profil'])){
			$log = '<li><span class="navbar-text"><a href='.$racine.'inscription> Inscrivez vous</a></span></li><li><span class="navbar-text"><a href='.$racine.'connexion> Connectez vous</a></span></li>';
		}else {
			$log = '<li><span class="navbar-text">Bienvenue '.$_SESSION['profil']['pseudo'].'</span></li><li><span class="navbar-text"><a href='.$racine.'deconnexion>Se deconnecter</a></span></li>';
			$ajout = '<li><a href="'.$racine.'ajouterPrestation">Ajout</a></li>';
		}
		
		$html =<<<END
		<!DOCTYPE html>
		<html>
		<head> 
			<link href="{$racine}public/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all" type="text/css">
			<link href="{$racine}public/Bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" media="all" type="text/css">
			<title>GIFTBOX</title> 
		</head>
		<body>
		<div class="container" style="padding-top:60px">
		<nav class="navbar navbar-default navbar-fixed-top">
		  <div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <span class="navbar-brand" >GIFTBOX</span>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">
				<li><a href="{$racine}.">Accueil</a></li>
				<li><a href="{$racine}prestations">Prestations</a></li>
				<li><a href="{$racine}categories">Catégories</a></li>
					{$ajout}
			</ul>
			  <ul class="nav navbar-nav navbar-right">
			  {$log}
				<li><a href="{$racine}coffret">Panier</a></li>
				
			  </ul>
			</div><!--/.nav-collapse -->
		  </div>
		</nav>
		 $content
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="{$racine}public/Bootstrap/dist/js/bootstrap.min.js"></script>
		</body><html>
END;
	return $html;
	}
}