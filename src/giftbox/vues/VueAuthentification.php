<?php 
namespace giftbox\vues;


class VueAuthentification{
	
	public $liste;
	public $aff;
	

	public function __construct($l, $affichage){
		$this->liste=$l;
		$this->aff=$affichage;
	}
	
	public function inscription() {
		$html = '<h1>INSCRIPTION ADMIN </h1><hr/><h3>on implique que toutes les personnes créant et utilisant des comptes sont des administrateurs (d\'après le sujet fonctionnalité 24)</h3>
		<form action="./creerutilisateur" method="post">
		<div style="width:170px; display:inline-block;">Pseudonyme : </div><input type="text" name="pseudo" required/><br />
		<div style="width:170px; display:inline-block;">Mot de passe : </div><input type="password" name="mdp" required/><br />
		<div style="width:170px; display:inline-block;">Répéter mot de passe : </div><input type="password" name="vmdp" required/><br />
		<div style="width:170px; display:inline-block;">Email : </div><input type="text" name="email" required/><br />
		<div style="width:170px; display:inline-block;">Répéter email : </div><input type="text" name="vemail" required/><br />
		<button class="btn btn-lg btn-primary btn-success" type="submit">Valider</button>
		</form>
		Il faut que le pseudonyme fasse plus de 8 caractères
		<br/>
		Il faut que le mot de passe fasse plus de 8 caractères et comporte au minimum une lettre et un chiffre
		<br/>
		<b>'.$this->liste[0].'</b>
		<br/>
		<a href ="./" > Retour accueil</a>';
		return $html;
	}
	
	public function connexion() {
		$html = '<h1>CONNEXION ADMIN</h1><hr/>
		<form action="./executerconnexion" method="post">
		<div style="width:130px; display:inline-block;">Pseudonyme : </div><input type="text" name="pseudo" required/><br />
		<div style="width:130px; display:inline-block;">Mot de passe : </div><input type="password" name="mdp" required/><br />
		<button class="btn btn-lg btn-primary btn-success" type="submit">Valider</button>
		<br/><br/>
		<b>'.$this->liste[0].'</b>
		</form>';
		return $html;
	}
	public function render(){
		switch($this->aff){
			case AFF_INSCRIPTION:
			$content = $this->inscription();
			$racine="";
			break;
			case AFF_CONNEXION:
			$content = $this->connexion();
			$racine="";
			break;
		}
		
		$html =<<<END
<!DOCTYPE html>
<html>
<head> 
	<link href="{$racine}public/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all" type="text/css">
	<link href="{$racine}public/Bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" media="all" type="text/css">
	<link href="{$racine}stylesheet.css" rel="stylesheet" media="all" type="text/css">
	<title>GIFTBOX</title> 
</head>
<body>
	<div class="container">

			$content
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="{$racine}public/Bootstrap/dist/js/bootstrap.min.js"></script>
</body>
<html>
END;
		return $html;
	}
}