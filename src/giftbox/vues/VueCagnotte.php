<?php

namespace giftbox\vues;
class VueCagnotte {
	
	public $liste;
	public $aff;
	

	public function __construct($l, $affichage){
		$this->liste=$l;
		$this->aff=$affichage;
	}
	private function gerer(){
		$content = '<h2>Gerer votre cagnotte </h2><table class="table">
			<tr>
				<th>Nom</th>
				<th>Prix</th>
				<th>Quantité</th>
				<th>Catégorie</th>
				<th>Vignette</th>

			</tr>';
			$prix=0;
		foreach($this->liste['prestations'] as $prestation){
			$content .='
								<tr>
								<td><a href="../../prestation/' . $prestation->id . '">' . $prestation->nom . '</a></td>
								<td>' . $prestation->prix . '€</td>
								<td>'.$prestation->pivot->qte.'  </td>
								<td>' . $prestation->categorie->nom . '</td>
								<td> <a href="../../prestation/' . $prestation->id . '"><img src="../../public/img/' . $prestation->img . '" style=" height: 150px; width: 200px;" alt="' . $prestation->img . '"/></a></td>
								</tr>';
								$prix+=($prestation->prix * $prestation->pivot->qte);
		}
		$content.='</table><h3>Prix minimum à atteindre : ' . $prix . '€</h3>';
		if($this->liste['etat']=='cloture'){
			$content.='<p style="font-size:18px;"">Vous avez cloturé cette cagnotte après avoir rassemblé ' . $this->liste['montant'] . '€ <br/>
			</p>';
		}else{
			$content.='<p style="font-size:18px;">Cette cagnotte a déja rassemblé ' . $this->liste['montant'] . '€ <br/>
			Vous pouvez participer en entrant un montant ci-dessous<br /></p>
			<form action="./' . basename("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") . '" method="post">
				<p style="font-size:25px;">Montant de votre participation : <input type="texte" name="montant"/>€</p>
				<button class="btn btn-lg btn-primary btn-success" type="submit">Participer</button>
			</form>';
			if($this->liste['montant']>=$prix){
				$content.='<br /> <p style="font-size:18px;">Vous avez atteint une somme suffisante pour cloturer votre cagnotte.<br />
				<a href="../cloturer/' . basename("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") . '"><button type="button" class="btn btn-lg btn-success">Cloturer ma cagnotte</button></a>';
			}
			$content.='</p>';
		}
		return $content;
	}
	
	private function participer(){
		$content = '<h2>Participer à une cagnotte </h2><table class="table">
			<tr>
				<th>Nom</th>
				<th>Prix</th>
				<th>Quantité</th>
				<th>Catégorie</th>
				<th>Vignette</th>

			</tr>';
			$prix=0;
		foreach($this->liste['prestations'] as $prestation){
			$content .='
								<tr>
								<td><a href="../../prestation/' . $prestation->id . '">' . $prestation->nom . '</a></td>
								<td>' . $prestation->prix . '€</td>
								<td>'.$prestation->pivot->qte.'  </td>
								<td>' . $prestation->categorie->nom . '</td>
								<td> <a href="../../prestation/' . $prestation->id . '"><img src="../../public/img/' . $prestation->img . '" style=" height: 150px; width: 200px;" alt="' . $prestation->img . '"/></a></td>
								</tr>';
								$prix+=($prestation->prix * $prestation->pivot->qte);
		}
		$content.='</table><h3>Prix minimum à atteindre : ' . $prix . '€</h3>';
		if($this->liste['etat']=='cloture'){
			$content.='<p style="font-size:18px;"">Cette cagnotte a été cloturée après avoir rassemblé ' . $this->liste['montant'] . '€ <br/>
			Merci à tous pour votre participation!</p>';
		}else{
			$content.='<p style="font-size:18px;"">Cette cagnotte a déja rassemblé ' . $this->liste['montant'] . '€ <br/>
			Vous pouvez participer en entrant un montant ci-dessous<br />
			<form action="./' . basename("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") . '" method="post">
				<p style="font-size:25px;">Montant de votre participation : <input type="texte" name="montant"/>€</p>
				<button class="btn btn-lg btn-primary btn-success" type="submit">Participer</button>
			</form></p>';
			
		}
		return $content;
	}
	
	public function render(){
		switch($this->aff){
			case PARTICIPER:
			$content = $this->participer();
			$racine="../../";
			break;
			case GERER:
			$content = $this->gerer();
			$racine='../../';
			break;
			case AFFICHER_ENSEMBLE:
			$content = $this->afficherEnsemble();
			$racine="../";
			break;
			case CADEAU_NON_DISPONIBLE:
			$content = $this->cadeauNonDisponible();
			$racine="../";
			break;
		}
		
		$log = "";
		$ajout = "";
		if(!isset($_SESSION['profil'])){
			$log = '<li><span class="navbar-text"><a href='.$racine.'inscription> Inscrivez vous</a></span></li><li><span class="navbar-text"><a href='.$racine.'connexion> Connectez vous</a></span></li>';
		}else {
			$log = '<li><span class="navbar-text">Bienvenue '.$_SESSION['profil']['pseudo'].'</span></li><li><span class="navbar-text"><a href='.$racine.'deconnexion>Se deconnecter</a></span></li>';
			$ajout = '<li><a href="'.$racine.'ajouterPrestation">Ajout</a></li>';
		}
		
		$html =<<<END
		<!DOCTYPE html>
		<html>
		<head> 
			<link href="{$racine}public/Bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="all" type="text/css">
			<link href="{$racine}public/Bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" media="all" type="text/css">
			<title>GIFTBOX</title> 
		</head>
		<body>
		<div class="container" style="padding-top:60px">
		<nav class="navbar navbar-default navbar-fixed-top">
		  <div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <span class="navbar-brand" >GIFTBOX</span>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">
				<li><a href="{$racine}.">Accueil</a></li>
				<li><a href="{$racine}prestations">Prestations</a></li>
				<li><a href="{$racine}categories">Catégories</a></li>
					{$ajout}
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
			  {$log}
				<li><a href="{$racine}coffret">Panier</a></li>
				
			  </ul>
			</div><!--/.nav-collapse -->
		  </div>
		</nav>
		 $content
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="{$racine}public/Bootstrap/dist/js/bootstrap.min.js"></script>
		</body><html>
END;
	return $html;
	}
}