<?php
namespace giftbox\controleurs;
use giftbox\models\Prestation;
use giftbox\models\Categorie;
use giftbox\models\Notations;
use Illuminate\Database\Capsule\Manager as DB;
use giftbox\securite\Authentication;

use giftbox\vues\VueCatalogue;


define('AFF_PRESTATIONS',1);
define('AFF_PRESTATION_PAR_ID',2);
define('AFF_CATEGORIES',3);
define('AFF_PRESTATIONS_PAR_CATEGORIES',4);  
define('AFF_FORMULAIRE_NOUVELLE_PRESTATION',5);  

 class CatalogueControleur{
	public function afficherPrestations() {
		if(isset($_SESSION['tri']) && $_SESSION['tri']=='decroissant'){
			if(Authentication::checkAccessRights(2)){
				$liste = Prestation::OrderBy('prix','DESC')->get();
			}else{
				$liste = Prestation::where('etat','=','active')->OrderBy('prix','DESC')->get();
			}
			
		}else{
			if(Authentication::checkAccessRights(2)){
				$liste = Prestation::OrderBy('prix')->get();
			}else{
				$liste = Prestation::where('etat','=','active')->OrderBy('prix')->get();
			}
		}
		$v = new VueCatalogue($liste,AFF_PRESTATIONS);
		return $v->render();
	}
	
	public function afficherPrestation($id){
		$prestation= Prestation::where('id','=',$id)->first();
		if($prestation->etat == 'inactive') {
			$app = \Slim\Slim::getInstance();
			$app->redirect('./../prestations');
		}else {
			$v = new VueCatalogue($prestation,AFF_PRESTATION_PAR_ID);
			return $v->render();
		}
	}
	
	public function afficherAccueil() {
		$listeCat = Categorie::get();
		$listeMeilleuresPrestations = array();
		
		foreach($listeCat as $cat){
			$noteMax = 0;
			$notes = DB::table("notations")
			->join('prestation','notations.prest_id','=','prestation.id')
			->join('categorie','categorie.id','=','prestation.cat_id')
			->where('categorie.id','=',$cat->id)
			->groupBy('prestation.id')->select(DB::raw('AVG( notations.note ) as moy'))->get();
			foreach($notes as $n){
				if($n['moy']>$noteMax){
					$noteMax = $n['moy'];
				}
			}
			$nomBestPrest = DB::table('prestation')
			->join('notations','notations.prest_id','=','prestation.id')
			->where("cat_id",'=',$cat->id)
			->havingRaw("avg(note)=".$noteMax)
			->groupBy("prestation.id")
			->select('nom')
			->first();
			$p = Prestation::where("nom","=",$nomBestPrest)->first();
			if(is_null($p)){
				$p = Prestation::where("cat_id",'=',$cat->id)->first();
			}
			if(!is_null($p)){
				$listeMeilleuresPrestations[] = $p; 
			}
			
		}
		$v = new VueCatalogue($listeMeilleuresPrestations, 0);
		return $v->render();
	}
	
	public function afficherCategories() {
		$cate = Categorie::get();
		$v = new VueCatalogue($cate,AFF_CATEGORIES);
		return $v->render();
	}
	
	public function afficherPrestParCat($nomCat){
		$cat = Categorie::where('nom','=',$nomCat)->first();
		if(isset($_SESSION['tri']) && $_SESSION['tri']=='decroissant'){
			$liste = $cat->prestations()->where('etat','=','active')->orderBy('prix','DESC')->get();
		}else{
			$liste = $cat->prestations()->where('etat','=','active')->orderBy('prix')->get();
		}
		$v = new VueCatalogue($liste,AFF_PRESTATIONS_PAR_CATEGORIES);
		return $v->render();
	}
	
	public function ajouterNote($idprest,$note) {
		if(!isset($_SESSION["notes"])) {
			$_SESSION["notes"] = array();
		}
		$newNote =  new Notations;
		$newNote->prest_id=$idprest;
		$newNote->note=$note;
		$newNote->save();
		$_SESSION["notes"][$idprest] = $note;
		$app = \Slim\Slim::getInstance();
		$app->redirect('../prestation/'.$idprest);
		
	}
	
	public function croissant(){
		$_SESSION["tri"]="croissant";
		$app = \Slim\Slim::getInstance();
		$app->redirect('./prestations');
	}
	
	public function decroissant(){
		$_SESSION["tri"]="decroissant";
		$app = \Slim\Slim::getInstance();
		$app->redirect('./prestations');
	}
	
	public function decroissantcat($nom){
		$_SESSION["tri"]="decroissant";
		$app = \Slim\Slim::getInstance();
		$app->redirect('./../../prestations/'.$nom);
	}
	
	public function croissantcat($nom){
		$_SESSION["tri"]="croissant";
		$app = \Slim\Slim::getInstance();
		$app->redirect('./../../prestations/'.$nom);
	}
	
	public function pageAjout() {
		if(Authentication::checkAccessRights(2)){
			$v = new VueCatalogue(null,AFF_FORMULAIRE_NOUVELLE_PRESTATION);
			return $v->render();
		}else {
			echo "Pas le niveau d'authentification suffisant pour faire ca";
		}
	}
	
	public function ajouterPrestation($n, $c, $p, $d, $i) {
		$app = \Slim\Slim::getInstance();
		if( filter_var( $p, FILTER_VALIDATE_FLOAT)){ //faire plus de filter mais FILTER_VALIDATE_STRING n'est pas reconnu
			$id_cat = Categorie::where('nom','=',$c)->select('id')->first()->id;
			$nprest = new Prestation();
			$nprest->nom = $n;
			$nprest->descr = $d;
			$nprest->prix = $p;
			$nprest->cat_id = $id_cat;
			$nprest->img = $i;
			$nprest->save();
			
			$id = Prestation::where('nom','=',$n)->first()->id;
			$app->redirect('./prestation/'.$id);
		}else {
			$app->redirect('./'); //redirige vers la ou tu veux quand on a fini l'ajout
		}
	}
	
	public function supprimerPrestation($id) {
		$app = \Slim\Slim::getInstance();
		$p = Prestation::where('id', '=', $id)->first();
		$no = Notations::where('prest_id', '=', $id)->get();
		foreach($no as $n){
			$n->delete();
		}
		$p->delete();
		$app->redirect('./../prestations'); 
	}
	
	public function desactiverPrestation($id) {
		$app = \Slim\Slim::getInstance();
		$p = Prestation::where('id', '=', $id)->first();
		$p->etat = 'inactive';
		$p->save();
		$app->redirect('./../prestations'); 
	}
	public function activerPrestation($id) {
		$app = \Slim\Slim::getInstance();
		$p = Prestation::where('id', '=', $id)->first();
		$p->etat = 'active';
		$p->save();
		$app->redirect('./../prestations'); 
	}
 }