<?php
namespace giftbox\controleurs;
use giftbox\vues\VueAuthentification;
use giftbox\securite\Authentication;
define('AFF_INSCRIPTION',1);
define('AFF_CONNEXION',2);

class AuthentificationControleur{
	
	public function inscription() {
		$vue = new VueAuthentification(null,AFF_INSCRIPTION);
		return $vue->render();
	}
	
	public function connexion() {
		$vue = new VueAuthentification(null,AFF_CONNEXION);
		return $vue->render();
	}
	
	public function deconnexion() {
		unset($_SESSION['profil']);
		$app = \Slim\Slim::getInstance();
		$app->redirect('./');
	}
	
	public function creerutilisateur($pseudo, $pswd, $vpswd, $email, $vemail) {
		$app = \Slim\Slim::getInstance();
		$pseudo =filter_var($pseudo,FILTER_SANITIZE_STRING);
		$pswd=filter_var($pswd,FILTER_SANITIZE_STRING);
		$vpswd=filter_var($vpswd,FILTER_SANITIZE_STRING);
		$email=filter_var($email,FILTER_SANITIZE_EMAIL);
		$vemail=filter_var($vemail,FILTER_SANITIZE_EMAIL);
		$mess = Authentication::createUser($pseudo,$pswd, $vpswd, $email, $vemail);
		if($mess != ""){
			$vue = new VueAuthentification(array($mess),AFF_INSCRIPTION);
			return $vue->render();
		}else {
			$app->redirect('./');
		}
	}
	
	public function executerconnexion($pseudo, $mdp) {
				$app = \Slim\Slim::getInstance();

		$pseudo =filter_var($pseudo,FILTER_SANITIZE_STRING);
		$mdp=filter_var($mdp,FILTER_SANITIZE_STRING);
		$mess = Authentication::authenticate($pseudo,$mdp);
		if($mess != ""){
			$vue = new VueAuthentification(array($mess),AFF_CONNEXION);
			return $vue->render();
		}else {
			$app->redirect('./');
		}
	}
}