<?php
namespace giftbox\controleurs;
use giftbox\models\Prestation;
use giftbox\models\Categorie;
use giftbox\models\Coffret;
use giftbox\vues\VuePanier;
use giftbox\vues\VueCadeau;
use giftbox\models\Cadeau;
use Illuminate\Database\Capsule\Manager as DB;
use DateTime;

define('AFFICHER_DEBUT_SEQUENCE',1);
define('AFFICHER_SEQUENCE',2);
define('AFFICHER_ENSEMBLE',3);
define('CADEAU_NON_DISPONIBLE',4);

class CadeauControleur {
	
	private function validateDate($date)
	{
		$d = DateTime::createFromFormat('Y-m-d', $date);
		return $d && $d->format('Y-m-d') === $date;
	}
	public function genererSequenceCadeau(){
		$app = \Slim\Slim::getInstance();
		if(isset($_SESSION['coffretGere'])){
			$idCadeau = rand ( 0,2147483647 );
			//on vérifie que l'id est bien libre, sinon on régénerre un id tant que ce n'est pas le cas
			$doublon = Cadeau::where('id','=',$idCadeau)->first();
			while($doublon!=null){
				$idCadeau = rand ( 0,2147483647 );
				$doublon = Cadeau::where('id','=',$idCadeau);
			}
			//on créer le nouveau cadeau
			$cadeau = new Cadeau();
			$cadeau->id=$idCadeau;
			$cadeau->dateVisibilite=null;
			$cadeau->etat='Généré, prêt à l\'envoie';
			$cadeau->coffret_id=$_SESSION['coffretGere'];
			$cadeau->save();
			$app->redirect('./gestion/'.$_SESSION['coffretGere']);
		}
		else{
			$app->redirect('./');
		}
	}
	
	public function genererCadeauDate($date){
		$app = \Slim\Slim::getInstance();
		//verification de la date
		if($this->validateDate($date, 'Y-m-d')){
			if(isset($_SESSION['coffretGere'])){
				$idCadeau = rand ( 0,2147483647 );
				//on vérifie que l'id est bien libre, sinon on régénerre un id tant que ce n'est pas le cas
				$doublon = Cadeau::where('id','=',$idCadeau)->first();
				while($doublon!=null){
					$idCadeau = rand ( 0,2147483647 );
					$doublon = Cadeau::where('id','=',$idCadeau);
				}
				//on créer le nouveau cadeau
				$cadeau = new Cadeau();
				$cadeau->id=$idCadeau;
				$cadeau->dateVisibilite=$date;
				$cadeau->etat='Généré, prêt à l\'envoie';
				$cadeau->coffret_id=$_SESSION['coffretGere'];
				$cadeau->save();
				$app->redirect('./gestion/'.$_SESSION['coffretGere']);
			}
			else{
				$app->redirect('./');
			}
		}
		else{
			$app->redirect('./');
		}
		
	}
	
	public function voirCadeau($id){
		$app = \Slim\Slim::getInstance();
		$cadeau =  Cadeau::where("id","=",$id)->first();
		if($cadeau!=null){
			$coffret = $cadeau->coffret;
			$prestations = $coffret->prestations;
			/*
			* Si on souhaite afficher les cadeau en sequence
			* On enregistre les differents id des presations dans la session, puis à l'aide d'une fonction suivant() on parcourera ce tableau jusqu'a la fin à l'aide d'un indice qui s'incrément de 1 en 1
			*/
			if($cadeau->dateVisibilite==null){
				$_SESSION['cadeaux']=array();
				$_SESSION['indiceCadeau']=0;
				$_SESSION['idCadeau']=$id;
				$i=0;
				foreach($prestations as $prestation){
					$_SESSION['cadeaux'][$i]=array('id'=>$prestation->id,'qte'=>$prestation->pivot->qte);
					$i++;
				}
				//modification de l'état du cadeau
				$cadeau->etat="En cours d'ouverture";
				//creation de la liste d'éléments
				$liste=array("nom"=>$coffret->nomCrea,"prenom"=>$coffret->prenomCrea,"message"=>$coffret->message);
				$v = new VueCadeau($liste,AFFICHER_DEBUT_SEQUENCE);
				return $v->render();
			}
			//si il y'a une date associé au cadeau
			else{
				if(time()<strtotime($cadeau->dateVisibilite)){
					$cadeau->etat='Reçu par le destinataire, non ouvert';
					$cadeau->save();
					$v = new VueCadeau(array("nom"=>$coffret->nomCrea,"prenom"=>$coffret->prenomCrea,"message"=>$coffret->message),CADEAU_NON_DISPONIBLE);
					return $v->render();
				}
				else{
					$cadeau->etat='Entièrement ouvert';
					$cadeau->save();
					$v = new VueCadeau(array("nom"=>$coffret->nomCrea,"prenom"=>$coffret->prenomCrea,"message"=>$coffret->message,"prestations"=>$prestations),AFFICHER_ENSEMBLE);
					return $v->render();
				}
			}
		}else{
			$app->redirect('./../');
		}
	}
	
	public function decouvrirCadeau(){
		$app = \Slim\Slim::getInstance();
		//on verifie que toute la session est prete pour voir le cadeau
		if(isset($_SESSION['indiceCadeau']) && isset($_SESSION['cadeaux']) && $_SESSION['indiceCadeau'] < count($_SESSION['cadeaux'])){
			//on verifie que les variables sont bien compatibles
			if(isset($_SESSION['cadeaux'][$_SESSION['indiceCadeau']])){
				$prestation = Prestation::where('id','=',$_SESSION['cadeaux'][$_SESSION['indiceCadeau']]['id'])->first();
				$v = new VueCadeau($prestation,AFFICHER_SEQUENCE);
				return $v->render();
			}
		}
		else{
			$app->redirect('./');
		}
	}
}