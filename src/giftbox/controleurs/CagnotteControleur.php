<?php
namespace giftbox\controleurs;
use giftbox\models\Prestation;
use giftbox\models\Categorie;
use giftbox\models\Coffret;
use giftbox\vues\VueCagnotte;
use giftbox\models\Cadeau;
use giftbox\models\Cagnotte;
use Illuminate\Database\Capsule\Manager as DB;

define('PARTICIPER',1);
define('GERER',2);


class CagnotteControleur {
	
	public function participer($id){
		$app = \Slim\Slim::getInstance();
		$cagnotte = Cagnotte::where('id','=',$id)->first();
		if($cagnotte!=null){
			$coffret=$cagnotte->coffret;
			$prestations=$coffret->prestations;
			$infos['prestations']=$prestations;
			$infos['montant']=$cagnotte->montantAtteind;
			$infos['etat']=$cagnotte->etat;
			$v = new VueCagnotte($infos,PARTICIPER);
			return $v->render();
			
		}
		else{
			$app->redirect($app->urlFor('accueil'));
		}
	}
	
	public function gerer($id){
		$app = \Slim\Slim::getInstance();
		$cagnotte = Cagnotte::where('id','=',$id)->first();
		if($cagnotte!=null){
			$coffret=$cagnotte->coffret;
			//verification des droits
			if($coffret->mdp=="" || (isset($_SESSION['mdp']) && password_verify ( $_SESSION['mdp'] , $coffret->mdp ))){   
				$prestations=$coffret->prestations;
				$infos['prestations']=$prestations;
				$infos['montant']=$cagnotte->montantAtteind;
				$infos['etat']=$cagnotte->etat;
				$v = new VueCagnotte($infos,GERER);
				return $v->render();
			}else{
				$app->redirect('../../coffret/verifie/' . $coffret->id);
			}
		}
		else{
			$app->redirect($app->urlFor('accueil'));
		}
	}
	
	public function AugmenterCagnote($id,$montant){
		$app = \Slim\Slim::getInstance();
		$cagnotte = Cagnotte::where('id','=',$id)->first();
		//verification des valeurs 
		if($cagnotte!=null &&  filter_var($montant, FILTER_VALIDATE_INT)){
			$cagnotte->montantAtteind+=$montant;
			$cagnotte->save();
			$app->redirect('./' . $id);
		}
		else{
			$app->redirect($app->urlFor('accueil'));
		}
	}
	
	public function AugmenterCagnoteGestion($id,$montant){
		$app = \Slim\Slim::getInstance();
		$cagnotte = Cagnotte::where('id','=',$id)->first();
		//verification des valeurs 
		if($cagnotte!=null &&  filter_var($montant, FILTER_VALIDATE_INT)){
			$coffret = $cagnotte->coffret;
			//verification des droits
			if($coffret->mdp=="" || (isset($_SESSION['mdp']) && password_verify ( $_SESSION['mdp'] , $coffret->mdp ))){   
				$cagnotte->montantAtteind+=$montant;
				$cagnotte->save();
				$app->redirect('./' . $id);
			}
			else{
				$app->redirect($app->urlFor('accueil'));
			}
		}
		else{
			$app->redirect($app->urlFor('accueil'));
		}
	}
	
	public function cloturerCagnotte($id){
		$app = \Slim\Slim::getInstance();
		$cagnotte = Cagnotte::where('id','=',$id)->first();
		//verification des valeurs 
		if($cagnotte!=null){
			$coffret = $cagnotte->coffret;
			//verification des droits
			if($coffret->mdp=="" || (isset($_SESSION['mdp']) && password_verify ( $_SESSION['mdp'] , $coffret->mdp ))){   
				$cagnotte->etat='cloture';
				$cagnotte->save();
				$app->redirect('../../gestion/' . $coffret->id);
			}
			else{
				$app->redirect($app->urlFor('accueil'));
			}
		}
		else{
			$app->redirect($app->urlFor('accueil'));
		}
	}
}