<?php
namespace giftbox\controleurs;
use giftbox\models\Prestation;
use giftbox\models\Categorie;
use giftbox\models\Coffret;
use giftbox\vues\VuePanier;
use giftbox\models\Cadeau;
use giftbox\models\Cagnotte;
use Illuminate\Database\Capsule\Manager as DB;

define('AFF_COURANT',1);
define('AFF_COFFRET_CB',2);
define('AFF_COFFRET_CAGNOTTE',3);
define('AFF_VALIDATION',4);
define('AFF_VERIFICATION',5);

class PanierControleur {
	
	
	
	public function afficherPanier() {
		$v = new VuePanier(null,AFF_COURANT);
		return $v->render();
	}
	
	public function ajouterPrestation($id){
		$app = \Slim\Slim::getInstance();
		//on verifie que le paramettre n'est pas n'importe quoi
		if ( filter_var( $id, FILTER_VALIDATE_INT) ) {
			//on verifie que le paramettre est bien dans la bdd 
			$prest= Prestation::where('id','=',$id)->first();
			if($prest != null){
				//on verifie si il n'y a pas déja un panier
				if(isset($_SESSION['panier'])){
					array_push($_SESSION['panier'], serialize($prest)); //array_push permet d'ajouter une valeur a la fin d'un tableau
					$app->redirect('./coffret');
				}
				//sinon on créer le panier
				else{
					$_SESSION['panier']=array();
					array_push($_SESSION['panier'], serialize($prest));
					$app->redirect('./coffret');
					
				}
			}
			else{
				$app->redirect('./');
				
			}
		}
		else{
				$app->redirect('./');
				
		}
	}
	
	public function supprimer($idPrest){
		$app = \Slim\Slim::getInstance();
		//on verifie que l'id de prestation n'est pas n'importe quoi
		if (filter_var( $idPrest, FILTER_VALIDATE_INT)) {
			//on verifie si on souhaite modifier le panier en construction
				//on cherche le premier éléments du panier qui à l'id correspondant et on le supprime
				if(isset($_SESSION['panier'])){
					foreach($_SESSION['panier'] as $index=>$element){     //$index=>$element permet de récuperer l'indice dans la boucle en plus de la valeur
						$prest = unserialize($element);
						if($prest->id == $idPrest){
							unset($_SESSION['panier'][$index]);
						
							break;
						}
					}
				}
				$app->redirect('./coffret');
				
			
		}
		else{
			$app->redirect('./');
			
		}
	}
	
	public function validation(){
		$app = \Slim\Slim::getInstance();
		$liste_qt=array();
		$categories=array();
		//on vérifie que la panier éxiste
		if(!isset($_SESSION['panier'])){
			$app->redirect('./coffret');
			$app->stop();
		}
		//creation de la liste des prestations
		foreach($_SESSION['panier'] as $element){
			if(isset($liste_qt[$element])){
				$liste_qt[$element]++;
			}
			else{
				$liste_qt[$element]=1;
				
			}
		}
		//creation d'un tableau contenant les catégorie (pour compter sa longueur)
		foreach($liste_qt as $prest=>$l){
			$p=unserialize($prest);
			$cat=$p->categorie->nom;
			if(isset($categories[$cat])){
				$categories[$cat]++;
			}
			else{
				$categories[$cat]=1;
			}
		}
		
		if(count($categories)>=2){
			$v = new VuePanier($liste_qt,AFF_VALIDATION);
			return $v->render();
		}
		else{
			$app->redirect('./coffret');
			
		}
		
	}
	
	public function validerCoffret($infos){
		$app = \Slim\Slim::getInstance();
		//on verifie si le pannier n'est pas vide
		if(!isset($_SESSION['panier'])){
			$app->redirect('./coffret');
			$app->stop();
		}
		//verification des données du formulaire
		if($infos['nom'] == "" || $infos['prenom'] == "" || $infos['message'] == "" || !filter_var( $infos['email'], FILTER_VALIDATE_EMAIL) || $infos['modePaiement'] == "") {
			$app->redirect('./validation');
			$app->stop();
			exit(); //au cas ou stop marche pas
		}
		//verif des donées bancaires, comme c'est un projet, on vérifie juste si ce n'est pas vide
		if($infos['modePaiement']=='classique'){
			if($infos['numCB']=="" || $infos['date']=="" || $infos['cryptogramme']==""){
			$app->redirect('./validation');
			$app->stop();
			exit();
			}
		}
		//sauvegarde du coffret
		$coffret=new Coffret();
		$coffret->nomCrea=htmlspecialchars($infos['nom']);
		$coffret->prenomCrea=htmlspecialchars($infos['prenom']);
		$coffret->emailCrea=htmlspecialchars($infos['email']);
		$coffret->message=htmlspecialchars($infos['message']);
		$coffret->modePaiement=htmlspecialchars($infos['modePaiement']);
		if($infos['mdp']==""){
			$coffret->mdp="";
		}else{
			$coffret->mdp=password_hash($infos['mdp'],PASSWORD_DEFAULT);
		}
		if($infos['modePaiement']=='classique'){
			$coffret->etat='payé';
			$coffret->save();
		}
		else{
			$coffret->etat='cagnotte en cours';
			$coffret->save();
			//creation de la cagnotte
			$cagnotte = new Cagnotte();
			$cagnotte->montantAtteind=0;
			$cagnotte->etat='En cours';
			$cagnotte->coffret_id=$coffret->id;
			$cagnotte->save();
		}
		

		//mise en relation avec les prestations
		$liste=array();
		foreach($_SESSION['panier'] as $element){
					$prest = unserialize($element);
					if(isset($liste_qt[$prest->id])){
						$liste_qt[$prest->id]++;
					}
					else{
						$liste_qt[$prest->id]=1;
						array_push($liste,$element); 
					}
		}
		foreach($liste as $prestation){
			$p = unserialize($prestation);
			//on utilise la façade DB pour la table coffret_prestation car ce n'est pas un objet, donc pas un model
			try{
				DB::beginTransaction();

				DB::table('coffret_prestation')->insert(['coffret_id' => $coffret->id,'prestation_id' => $p->id, 'qte'=>$liste_qt[$p->id]]);
				DB::commit();
			}catch(\Exception $e){
				DB::rollback();
			}
		}
		//on vide le panier
		unset($_SESSION['panier']);
		$app->redirect('./gestion/'.$coffret->id);
		
	}
	
	public function voirCoffret($id){
		$app = \Slim\Slim::getInstance();
		if ( filter_var( $id, FILTER_VALIDATE_INT) ) {
			$coffret = Coffret::where("id","=",$id)->first();
			if($coffret!=null){
				//on vérifie si on à accès au coffret ou si il est en accès libre
				if($coffret->mdp=="" || (isset($_SESSION['mdp']) && password_verify ( $_SESSION['mdp'] , $coffret->mdp ))){   
					$prestations=$coffret->prestations;  //les objet dans $prestations contiennent les attribut d'une prestation ainsi que l'attribut ->pivot->qte 
					if($coffret->modePaiement=='classique'){
						$v = new VuePanier($prestations,AFF_COFFRET_CB);
						$_SESSION['coffretGere']=$id;
						return $v->render();
					}
					elseif($coffret->modePaiement=='cagnotte'){
					$v = new VuePanier($prestations,AFF_COFFRET_CAGNOTTE);
					$_SESSION['coffretGere']=$id;
					return $v->render();
					}
				}
				//si on a pas d'accès on est rediriger vers la page de vérification
				else{
					$app->redirect('./../coffret/verifie/' . $id );
					
				}
				
			}
			else{
				$app->redirect('./../' );
				
			}
		}
		
	}
	
	public function passeCoffret($id){
		$app = \Slim\Slim::getInstance();
		if ( filter_var( $id, FILTER_VALIDATE_INT) ){
			$coffret = Coffret::where("id","=",$id)->first();
			if($coffret!=null && $coffret->mdp!=""){
				$v = new VuePanier($coffret,AFF_VERIFICATION);
				return $v->render();
				
			}
			else{
				$app->redirect('./../');
				
			}
		}
	}
	
	public function verifierMDP($id,$mdp){
		$app = \Slim\Slim::getInstance();
		if ( filter_var( $id, FILTER_VALIDATE_INT) ){
			$coffret = Coffret::where("id","=",$id)->first();
			if($coffret!=null && $coffret->mdp!=""){
				if(password_verify ( $mdp , $coffret->mdp )){
					$_SESSION['mdp']=$mdp;
				}
				$app->redirect('./../../gestion/' . $id );
				
			}
			else{
				$app->redirect('./../../');
			}
			
		
		}
	}
}
