<?php 
namespace giftbox\models;


class Notations extends \Illuminate\Database\Eloquent\Model{
	
	protected $table="notations";
	protected $primaryKey = 'id' ;
	public $timestamps = false;
	
	public function prestation(){
		return $this->belongsTo('\giftbox\models\Notation','prest_id');
	}
}