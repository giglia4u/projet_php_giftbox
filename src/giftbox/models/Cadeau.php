<?php 
namespace giftbox\models;

class Cadeau extends \Illuminate\Database\Eloquent\Model{
	
	protected $table="cadeau";
	protected $primaryKey = 'id' ;
	public $timestamps = false;
	
	public function coffret()
    {
		
        return $this->belongsTo('\giftbox\models\Coffret', 'coffret_id');  
    }
	
	
}