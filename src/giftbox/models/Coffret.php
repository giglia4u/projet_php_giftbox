<?php 
namespace giftbox\models;

class Coffret extends \Illuminate\Database\Eloquent\Model{
	
	protected $table="coffret";
	protected $primaryKey = 'id' ;
	public $timestamps = false;
	
	public function prestations()
    {
		/*
		*  Le pivot permet de récuperer les collones intermédiaire entre les deux collones, comme l'attribut quantité
		*  On accès au pivot grace à l'attribut ->pivot des prestations retournés, ->pivot->qte pour avoit la quantité
		*/
        return $this->belongsToMany('\giftbox\models\Prestation', 'coffret_prestation', 'coffret_id', 'prestation_id')->withPivot('qte');  
    }
	
	public function cagnotte(){
		return $this->hasOne('\giftbox\models\Cagnotte', 'coffret_id');  
	}
	
	
}