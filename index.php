﻿<?php
require_once 'vendor/autoload.php';
use giftbox\models\Prestation;
use giftbox\models\Categorie;

use Illuminate\Database\Capsule\Manager as DB;
ob_start();
$info=parse_ini_file("src/conf/conf.ini");
$db = new DB();
$db->addConnection( [
 'driver' => $info["driver"],
 'host' => $info["host"],
 'database' => $info["database"],
 'username' => $info['username'],
 'password' => $info['password'],
 'charset' => 'utf8',
 'collation' => 'utf8_unicode_ci',
 'prefix' => ''
] );
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

$app->get('/',function(){
 $cont = new giftbox\controleurs\CatalogueControleur();
 echo $cont->afficherAccueil();
})->name("accueil");

$app->get('/coffret', function(){
 $cont = new giftbox\controleurs\PanierControleur();
 echo $cont->afficherPanier();
});
$app->get('/prestations', function() {
 $cont = new giftbox\controleurs\CatalogueControleur();
 echo $cont->afficherPrestations();
});

$app->get('/prestation/:id',function($id) {
 $cont = new giftbox\controleurs\CatalogueControleur();
 echo $cont->afficherPrestation($id);
 
});

$app->get('/categories',function() {
 $cont = new giftbox\controleurs\CatalogueControleur();
 echo $cont->afficherCategories();
});

$app->get('/prestations/:nomCat',function($nomCat) {
 $cont = new giftbox\controleurs\CatalogueControleur();
 echo $cont->afficherPrestParCat($nomCat);
});

$app->post('/note/:idprest' , function($idprest){
	$app = \Slim\Slim::getInstance();
	$ajouteurNote = new giftbox\controleurs\CatalogueControleur();
	$ajouteurNote->ajouterNote($idprest,$app->request->post('rating'));
});

$app->post('/ajout' , function(){
	$app = \Slim\Slim::getInstance();
	$ajouteur = new giftbox\controleurs\PanierControleur();
	$ajouteur->ajouterPrestation($app->request->post('bouton'));
});

$app->get('/coffret', function(){
	$cont = new giftbox\controleurs\PanierControleur();
	echo $cont->afficherPanier();
});

$app->post('/suppression',function(){
	$app = $app = \Slim\Slim::getInstance();
	$gestion = new giftbox\controleurs\PanierControleur();
	$gestion->supprimer($app->request->post('bouton'));
});

$app->get('/gestion/:idPanier',function($idPanier){
	$gestion = new giftbox\controleurs\PanierControleur();
	echo $gestion->voirCoffret($idPanier);
});

$app->get('/validation',function(){
	$validateur = new giftbox\controleurs\PanierControleur();
	echo $validateur->validation();
});

$app->post('/validation', function(){
	$app = $app = \Slim\Slim::getInstance();
	$validateur = new giftbox\controleurs\PanierControleur();
	$validateur->validerCoffret(array(
	"nom" => $app->request->post('nom'), 
	"prenom"=>$app->request->post('prenom'),
	"email"=>$app->request->post('email'),
	"message"=>$app->request->post('message'),
	"modePaiement"=>$app->request->post('modePaiement'),
	"mdp"=>$app->request->post('mdp'),
	"numCB"=>$app->request->post('numCB'),
	"date"=>$app->request->post('date'),
	"cryptogramme"=>$app->request->post('cryptogramme')
	));
	
});

$app->get('/coffret/verifie/:id',function($id){
	$verficateur = new giftbox\controleurs\PanierControleur();
	echo $verficateur->passeCoffret($id);
});

$app->post('/coffret/verifie/:id',function($id){
	$app = $app = \Slim\Slim::getInstance();
	$verficateur = new giftbox\controleurs\PanierControleur();
	$verficateur->verifierMDP($id, $app->request->post('mdp'));
});

$app->get('/generation-cadeau',function(){
	$cad = new giftbox\controleurs\CadeauControleur();
	$cad->genererSequenceCadeau();
});

$app->post('/generation-cadeau',function(){
	$app = \Slim\Slim::getInstance();
	$cad = new giftbox\controleurs\CadeauControleur();
	$cad->genererCadeauDate($app->request->post('date'));
});

$app->get('/cadeau/:id',function($id){
	$cad = new giftbox\controleurs\CadeauControleur();
	echo $cad->voirCadeau($id);
})->name('cadeau');

$app->get('/decouvrir-cadeau',function(){
	$cad = new giftbox\controleurs\CadeauControleur();
	echo $cad->decouvrirCadeau();
});

$app->get('/croissant',function(){
	$cont = new giftbox\controleurs\CatalogueControleur();
	$cont->croissant();
});

$app->get('/decroissant',function(){
	$cont = new giftbox\controleurs\CatalogueControleur();
	$cont->decroissant();
});

$app->get('/prestations/decroissant/:nom',function($nom){
	$cont = new giftbox\controleurs\CatalogueControleur();
	$cont->decroissantcat($nom);
});

$app->get('/prestations/croissant/:nom',function($nom){
	$cont = new giftbox\controleurs\CatalogueControleur();
	$cont->croissantcat($nom);
});

$app->get('/cagnotte/participation/:id',function($id){
	$cont = new giftbox\controleurs\CagnotteControleur();
	echo $cont->participer($id);
})->name('participation');

$app->post('/cagnotte/participation/:id',function($id){
	$app = \Slim\Slim::getInstance();
	$cont = new giftbox\controleurs\CagnotteControleur();
	$cont->AugmenterCagnote($id,$app->request->post('montant'));
});

$app->get('/cagnotte/gestion/:id',function($id){
	$cont = new giftbox\controleurs\CagnotteControleur();
	echo $cont->gerer($id);
})->name('gestionCagnotte');

$app->post('/cagnotte/gestion/:id',function($id){
	$app = \Slim\Slim::getInstance();
	$cont = new giftbox\controleurs\CagnotteControleur();
	$cont->AugmenterCagnoteGestion($id,$app->request->post('montant'));
});

$app->get('/cagnotte/cloturer/:id',function($id){
	$app = \Slim\Slim::getInstance();
	$cont = new giftbox\controleurs\CagnotteControleur();
	$cont->cloturerCagnotte($id);
});

$app->get('/inscription', function() {
	$app = \Slim\Slim::getInstance();
	$cont = new giftbox\controleurs\AuthentificationControleur();
	echo $cont->inscription();
});

$app->post('/creerutilisateur', function() {
	$app = \Slim\Slim::getInstance();
	$cont = new giftbox\controleurs\AuthentificationControleur();
	echo $cont->creerutilisateur($app->request->post('pseudo'),$app->request->post('mdp'),$app->request->post('vmdp'),$app->request->post('email'),$app->request->post('vemail'));
});

$app->get('/connexion', function() {
	$app = \Slim\Slim::getInstance();
	$cont = new giftbox\controleurs\AuthentificationControleur();
	echo $cont->connexion();
});

$app->post('/executerconnexion', function() {
	$app = \Slim\Slim::getInstance();
	$cont = new giftbox\controleurs\AuthentificationControleur();
	echo $cont->executerconnexion($app->request->post('pseudo'),$app->request->post('mdp'));
});

$app->get('/deconnexion', function() {
	$app = \Slim\Slim::getInstance();
	$cont = new giftbox\controleurs\AuthentificationControleur();
	echo $cont->deconnexion();
});

$app->get('/ajouterPrestation', function() {
	$app = \Slim\Slim::getInstance();
	$cont = new giftbox\controleurs\CatalogueControleur();
	echo $cont->pageAjout();
});

$app->post('/ajouterPrestation', function() {
	$app = \Slim\Slim::getInstance();
	$cont = new giftbox\controleurs\CatalogueControleur();
	move_uploaded_file($_FILES['image']['tmp_name'],'public/img/'.$_FILES['image']['name'] );
	echo $cont->ajouterPrestation($app->request->post('nom'),$app->request->post('categorie'),$app->request->post('prix'),$app->request->post('description'),$_FILES['image']['name']);
});

$app->get('/supprimerPrestation/:id',function($id){
	$app = \Slim\Slim::getInstance();
	$cont = new giftbox\controleurs\CatalogueControleur();
	$cont->supprimerPrestation($id);
});

$app->get('/desactivePrestation/:id',function($id){
	$app = \Slim\Slim::getInstance();
	$cont = new giftbox\controleurs\CatalogueControleur();
	$cont->desactiverPrestation($id);
});

$app->get('/activePrestation/:id',function($id){
	$app = \Slim\Slim::getInstance();
	$cont = new giftbox\controleurs\CatalogueControleur();
	$cont->activerPrestation($id);
});

session_start();
$app->run();